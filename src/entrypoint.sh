#!/bin/sh
inotifywait -e close_write,moved_to,create -m $CONFIG_DIR |
while read -r directory events filename; do
  killall -SIGHUP quasselcore
done
