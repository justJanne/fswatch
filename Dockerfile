FROM alpine:3.15

RUN apk add --no-cache \
  inotify-tools
  
ADD src/entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
